# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into an image with no system at all.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.
# Your binary must be statically compiled with no dynamic dependencies on system libraries.
# e.g. for Docker:
# CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

#
# Nginx Dockerfile
#
# https://github.com/dockerfile/nginx
#

# Pull base image.
FROM dockerfile/ubuntu

COPY index.html /tmp
COPY nginx.conf /tmp

# Install Nginx.
RUN \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && \
  apt-get install -y nginx && \
  rm -rf /var/lib/apt/lists/* && \
  echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
  chown -R www-data:www-data /var/lib/nginx && \
  ls -ltr /etc/nginx/nginx.conf && \
  cat /etc/nginx/nginx.conf && \
  cp -p /tmp/nginx.conf /etc/nginx/nginx.conf && \
  cat /etc/nginx/nginx.conf && \
  cp /tmp/index.html /etc/nginx/sites-enabled && \
  ls -ltr /var/lib/nginx && \
  ls -ltr /etc/nginx && \
  ls -ltr /etc/init.d && \
  /etc/init.d/nginx restart
  
  

# Define mountable directories.
VOLUME ["/etc/nginx/sites-enabled", "/etc/nginx/certs", "/etc/nginx/conf.d", "/var/log/nginx", "/var/www/html"]

# Define working directory.
WORKDIR /etc/nginx

# Define default command.
CMD ["nginx"]

# Expose ports.
EXPOSE 80
EXPOSE 443
